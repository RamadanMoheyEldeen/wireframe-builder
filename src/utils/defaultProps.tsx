import React from 'react'
import * as Chakra from '@chakra-ui/react'

import {
  BoxProps,
  HeadingProps,
  FlexProps,
  GridProps,
  TextProps,
  AspectRatioProps,
  ImageProps,
  ButtonProps
} from '@chakra-ui/react'

type PropsWithForm<T> = T & { form?: T }

type PreviewDefaultProps = {
  Box?: PropsWithForm<BoxProps>
  Text?: PropsWithForm<TextProps>
  Heading?: PropsWithForm<HeadingProps>
  Flex?: PropsWithForm<FlexProps>
  Grid?: PropsWithForm<GridProps>
  AspectRatio?: PropsWithForm<AspectRatioProps>
  Image?: PropsWithForm<ImageProps>
  Button?: PropsWithForm<ButtonProps>
}

export const DEFAULT_PROPS: PreviewDefaultProps = {
  Flex: {
    flexDirection: 'column',
    form: {
      display: 'flex',
    },
  },
  AspectRatio: {
    height: 300
  },
  Grid: {
    gap: 3,
    form: {
      display: 'grid',
    },
  },
  Heading: {
    children: 'Heading title',
    as: 'h5'
  },
  Text: { children: 'Text value' },
  Image: {
    height: '200px',
    width: '300px'
  },
  Button: {
    children: 'Click here'
  }
}

export const getDefaultFormProps = (type: ComponentType) => {
  //@ts-ignore
  const chakraDefaultProps = Chakra[type]?.defaultProps || {}
  // @ts-ignore
  return { ...chakraDefaultProps, ...DEFAULT_PROPS[type]?.form }
}
