const getBoxes = (components: any) => {
  return Object.entries(components)
    .map(item => item[1])
    .filter((item: any) => item.id != 'root' && item.type == 'Box')
}

const getRows = (components: any, id: string) => {
  return Object.entries(components)
    .map(item => item[1])
    .filter(
      (item: any) =>
        item.id != 'root' && item.type == 'Grid' && item.parent == id,
    )
}

const getColumns = (components: any, id: string) => {
  return Object.entries(components)
    .map(item => item[1])
    .filter(
      (item: any) =>
        item.id != 'root' && item.type == 'Flex' && item.parent == id,
    )
}

const getColumnWidth = (row: any, columnIndex: any) => {
  let width = 0
  if(row.props?.templateColumns)
       switch(row.props.templateColumns){
         case "repeat(1, 1fr)":
          width = 100;
          break;
         case "repeat(2, 1fr)":
          width = 50
          break;
         case "repeat(3, 1fr)":
          width = 33.33
          break;
         case "1fr 2fr":
          width = columnIndex % 2 == 0 ? 33.33 : 66.66;
          break;
         case "2fr 1fr":
          width = columnIndex % 2 != 0 ? 33.33 : 66.66;
          break;
          default:
            width = 100
       }
  
  return `"width":"${width}","width-unit":"%"`
}

const getItems = (components: any, id: string) => {
  return Object.entries(components)
    .map(item => item[1])
    .filter((item: any) => item.parent == id)
}

const getItemCode = (item: any, itemId: any, parentId: any, pageId: any) => {
  let  classes = item.props && item.props.className ? `,"activeselector":"${item.props.className}","classes":{"0":"${item.props.className}"}` : ''
  switch (item.type) {
    case 'Heading':
      return `
                                      [ct_headline  ct_options='{"ct_id":${itemId},"ct_parent":${parentId},"selector":"headline-${itemId}-${pageId}","original":{"tag":"${item.props.headingType}"}} ${classes}']
                                          ${item.props.children}
                                      [/ct_headline]
    
          `
    case 'Image':
      return `
                                      [ct_image ct_options='{"ct_id":${itemId},"ct_parent":${parentId},"selector":"image-${itemId}-${pageId}","original":{"src":"${item.props.src},"width-unit":"%"} ${classes}']
                                      [/ct_image]
      `
    case 'AspectRatio':
      return `
                                      [ct_video  ct_options='{"ct_id":${itemId},"ct_parent":${parentId},"selector":"video-${itemId}-${pageId}","original":{"src":${item.props.src},"embed_src":${item.props.src}}} ${classes}']
                                      [/ct_video]
      `
    case 'Text':
      return                          `[ct_text_block ct_options='{"ct_id":${itemId},"ct_parent":${parentId},"selector":"text-${itemId}-${pageId}"} ${classes}']
                                               ${item.props.children}
                                       [/ct_text_block]`
    case 'Button':
       return  `
                                        [ct_link_button  ct_options='{"ct_id":25,"ct_parent":1,"selector":"link_button-25-240","original":{"url":${item.props.url},"url_encoded":"true"},"activeselector": "${item.props.className}","classes":{"0": "${item.props.className}"}}} ${classes}']
                                                ${item.props.children}
                                        [/ct_link_button]
        `
      default: 
        return ''
  }
}

export const generateCode = async (components: IComponents) => {
  let pageId = localStorage.getItem('pageId')

  let code = getBoxes(components).map((sectionItem: any, sectionIndex) => {
    let sectionId = sectionIndex + 1;
    let sectionClass = sectionItem.props && sectionItem.props.className ? `,"activeselector":"${sectionItem.props.className}","classes":{"0":"${sectionItem.props.className}"}` : ''
    return `
  [ct_section ct_options='{"ct_id": ${sectionId}, "ct_parent":0,"selector":"section-${sectionId}-${pageId}"} ${sectionClass}']
     ${getRows(components, sectionItem.id)
       .map((rowItem: any, rowIndex) => {
         let rowId = sectionId + '' + rowIndex;
         let rowClass = rowItem.props && rowItem.props.className ? `,"activeselector":"${rowItem.props.className}","classes":{"0":"${rowItem.props.className}"}` : ''
         return `
                [ct_new_columns_2  ct_options='{"ct_id": ${rowId},"ct_parent": ${sectionId},"selector":"new_columns-${rowId}-${pageId}"} ${rowClass}']
                    ${getColumns(components, rowItem.id)
                      .map((columnItem: any, columnIndex) => {
                        let columnId =
                          sectionId + '' + rowIndex + '' + columnIndex;
                          let columnClass = columnItem.props && columnItem.props.className ? `,"activeselector":"${columnItem.props.className}","classes":{"0":"${columnItem.props.className}"}` : ''
                          let width = getColumnWidth(rowItem, columnIndex)

                        return `
                                   [ct_div_block_2  ct_options='{"ct_id":${columnId},"ct_parent": ${rowId},"selector":"div_block-${rowId}-${pageId}","original":{${width}} ${columnClass}}' ]
                                      ${getItems(components, columnItem.id)
                                        .map((item: any, index) => {
                                          let itemId = columnId + '' + index
                                          return getItemCode(
                                            item,
                                            itemId,
                                            columnId,
                                            pageId,
                                          )
                                        })
                                        .join('')}
                                   [/ct_div_block_2]
                                 `
                      })
                      .join('')}
                [/ct_new_columns_2]
              `
       })
       .join('')}
  [/ct_section]
  `
  })

  console.log('Boxes', getBoxes(components))
  console.log('Rows', getRows(components, 'comp-KNI2UULCE7PQ9'))
  console.log('Columns', getColumns(components, 'comp-KNI2V4HC135RG'))
  console.log('Columns', getColumns(components, 'comp-KNJPKDZKNHPUJ'))
  console.log('Items', getItems(components, 'comp-KNI2V5PR7R7RZ'))

  console.log({ ...components })
  return code.join('')
}
