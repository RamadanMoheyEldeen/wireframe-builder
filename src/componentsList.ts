export type MenuItem = {
  children?: MenuItems
  soon?: boolean
  rootParentType?: ComponentType
}

type MenuItems = Partial<
  {
    [k in ComponentType]: MenuItem
  }
>

export const menuItems: MenuItems = {
  Box: {},
  Grid: {},
  Flex: {},
  Heading: {},
  Text: {},
  AspectRatio: {},
  Image: {},
  Button: {}
}

export const componentsList: ComponentType[] = [
  'Box',
  'Grid',
  'Flex',
  'Heading',
  'Text',
  'AspectRatio',
  'Image',
  'Button'
]
