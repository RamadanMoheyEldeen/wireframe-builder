import React, { ReactNode } from 'react'
import { Select } from '@chakra-ui/react'
import FormControl from './FormControl'
import { useForm } from '~hooks/useForm'
import usePropsSelector from '~hooks/usePropsSelector'

type SelectControl = {
  name: string
  label: string | ReactNode
}

const SelectControl: React.FC<SelectControl> = ({ name, label }) => {
  const { setValueFromEvent } = useForm()
  const value = usePropsSelector(name)

  return (
    <FormControl label={label} htmlFor={name}>
      <Select
        name={name}
        id={name}
        size="sm"
        onChange={setValueFromEvent}
      >
        <option value="repeat(1, 1fr)">1 column</option>
        <option value="repeat(2, 1fr)">2 columns - 1⁄2 + 1⁄2</option>
        <option value="repeat(3, 1fr)">3 columns - 1⁄3 + 1⁄3 + 1⁄3</option>
        <option value="1fr 2fr">2 columns - 1⁄3 + 2⁄3</option>
        <option value="2fr 1fr">2 columns - 2⁄3 + 1⁄3</option>
      </Select>
    </FormControl>
  )
}

export default SelectControl
