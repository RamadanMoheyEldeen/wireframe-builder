import React, { ReactNode } from 'react'
import { Select } from '@chakra-ui/react'
import FormControl from './FormControl'
import { useForm } from '~hooks/useForm'
import usePropsSelector from '~hooks/usePropsSelector'

type SelectControl = {
  name: string
  label: string | ReactNode
}

const HeadingSelector: React.FC<SelectControl> = ({ name, label }) => {
  const { setValueFromEvent } = useForm()
  const value = usePropsSelector(name)

  return (
    <FormControl label={label} htmlFor={name}>
      <Select
        name={name}
        id={name}
        onChange={setValueFromEvent}
      >
        <option value="h1">h1</option>
        <option value="h2">h2</option>
        <option value="h3">h3</option>
        <option value="h4">h4</option>
      </Select>
    </FormControl>
  )
}

export default HeadingSelector
