import React, { memo } from 'react'
import ColorsControl from '~components/inspector/controls/ColorsControl'
import TextControl from '~components/inspector/controls/TextControl'

const BoxPanel = () => (
<>
  <TextControl label="Class Name" name='className'/>
  <ColorsControl
    withFullColor
    label="Color"
    name="backgroundColor"
    enableHues
  />
  </>
)

export default memo(BoxPanel)
