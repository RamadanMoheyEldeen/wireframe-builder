import React, { memo } from 'react'
import ColorsControl from '~components/inspector/controls/ColorsControl'

const Section = () => (
  <ColorsControl
    withFullColor
    label="Color"
    name="backgroundColor"
    enableHues
  />
)

export default memo(Section)
