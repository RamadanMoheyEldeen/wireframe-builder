import React, { memo } from 'react'
import { Select } from '@chakra-ui/react'
import { useForm } from '~hooks/useForm'
import HeadingSelector from '~components/inspector/controls/HeadingSelector'
import ChildrenControl from '~components/inspector/controls/ChildrenControl'
import usePropsSelector from '~hooks/usePropsSelector'
import TextControl from '~components/inspector/controls/TextControl'

const HeadingPanel = () => {

  return (
    <>
      <ChildrenControl />
      <HeadingSelector label="Heading type" name="headingType"/>

    </>
  )
}

export default memo(HeadingPanel)
