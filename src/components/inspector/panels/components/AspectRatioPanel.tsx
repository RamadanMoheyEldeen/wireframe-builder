import React from 'react'
import TextControl from '~components/inspector/controls/TextControl'
import SwitchControl from '~components/inspector/controls/SwitchControl'


const AspectRatioPanel = () => {
  return (
    <>
      <TextControl name="src" label="Video source"/>
      <TextControl label="Class Name" name='className'/>
    </>
  )
}

export default AspectRatioPanel
