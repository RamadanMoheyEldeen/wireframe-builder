import React, { memo } from 'react'
import { useSelector } from 'react-redux'
import * as Chakra from '@chakra-ui/react'
import { getComponentBy } from '~core/selectors/components'
import PreviewContainer from '~components/editor/PreviewContainer'
import WithChildrenPreviewContainer from '~components/editor/WithChildrenPreviewContainer'
import AspectRatioPreview from '~components/editor/previews/AspectRatioBoxPreview'

const ComponentPreview: React.FC<{
  componentName: string
}> = ({ componentName, ...forwardedProps }) => {
  const component = useSelector(getComponentBy(componentName))
  if (!component) {
    console.error(`ComponentPreview unavailable for component ${componentName}`)
  }

  const type = (component && component.type) || null
  
  switch (type) {
    case 'Text':
    case 'Heading':
    case 'Image':
    case 'Button':
    return (
        <PreviewContainer
          component={component}
          type={Chakra[type]}
          {...forwardedProps}
        />
      )
    case 'Box':
    case 'Flex':
    case 'Grid':
      return (
        <WithChildrenPreviewContainer
          enableVisualHelper
          component={component}
          type={Chakra[type]}
          {...forwardedProps}
        />
      )
    case 'AspectRatio':
      return <AspectRatioPreview component={component} />
    default:
      return null
  }
}

export default memo(ComponentPreview)
