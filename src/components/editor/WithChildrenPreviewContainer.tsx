import React, { FunctionComponent, ComponentClass } from 'react'
import { useInteractive } from '~hooks/useInteractive'
import { useDropComponent } from '~hooks/useDropComponent'
import ComponentPreview from '~components/editor/ComponentPreview'
import { Box } from '@chakra-ui/react'

const WithChildrenPreviewContainer: React.FC<{
  component: IComponent
  type: string | FunctionComponent<any> | ComponentClass<any, any>
  enableVisualHelper?: boolean
  isBoxWrapped?: boolean
}> = ({
  component,
  type,
  enableVisualHelper = false,
  isBoxWrapped,
  ...forwardedProps
}) => {
  //you have to add what component you want to add to column in this array
  let allowed = ['Text', 'Heading', 'Image', 'AspectRatio', 'Button']
  // if the parent is section just allow to have only rows
  if (component.rootParentType == 'Box') allowed = ['Grid']
  // if the parent is Row just allow to have only columns
  else if (component.rootParentType == 'Grid') allowed = ['Flex']

  const allowedList: (ComponentType | MetaComponentType)[] = allowed
  const { drop, isOver } = useDropComponent(component.id, allowedList)
  const { props, ref } = useInteractive(component, enableVisualHelper)
  const propsElement = { ...props, ...forwardedProps, pos: 'relative' }

  if (!isBoxWrapped) {
    propsElement.ref = drop(ref)
  }

  if (isOver) {
    propsElement.bg = 'teal.50'
  }
  const children = React.createElement(
    type,
    propsElement,
    component.children.map((key: string) => {
      return <ComponentPreview key={key} componentName={key} />
    }),
  )

  if (isBoxWrapped) {
    let boxProps: any = {
      display: 'inline',
    }

    return (
      <Box {...boxProps} ref={drop(ref)}>
        {children}
      </Box>
    )
  }

  return children
}

export default WithChildrenPreviewContainer
