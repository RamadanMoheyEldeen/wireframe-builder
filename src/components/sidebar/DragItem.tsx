import React from 'react'
import { useDrag } from 'react-dnd'
import { Text, Box } from '@chakra-ui/react'
import { getCustomName } from '~utils/customs'

const DragItem: React.FC<ComponentItemProps> = ({
  type,
  soon,
  label,
  isMeta,
  isChild,
  rootParentType,
}) => {
  const [, drag] = useDrag({
    item: { id: type, type, isMeta, rootParentType },
  })

  let boxProps: any = {
    color: 'whiteAlpha.600',
  }

  if (!soon) {
    boxProps = {
      ref: drag,
      color: 'whiteAlpha.800',
      cursor: 'move',
      _hover: {
        ml: -1,
        mr: 1,
        bg: '#E8E8E8',
        boxShadow: 'sm',
        color: 'white',
      },
    }
  }

  if (isChild) {
    boxProps = { ...boxProps, ml: 4 }
  }


  const labelName = getCustomName(label)
  return (
    <Box
      boxSizing="border-box"
      transition="margin 200ms"
      p={2}
      display="flex"
      alignItems="center"
      {...boxProps}
    >
      <Text letterSpacing="wide" fontSize="sm" color="black" textTransform="capitalize">
        {labelName} 
      </Text>
    </Box>
  )
}

export default DragItem
